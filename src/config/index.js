
import Vue from 'vue'
import vueConfig from 'vue-config'

Vue.config.productionTip = process.env.NODE_ENV === 'production'
const baseConfigs = {
  GOOGLE_CLIENT: {
    clientId: '914053341446-pd8ldg7k5dck5mhi5a2dd82hunmmv7f9.apps.googleusercontent.com',
    scope: 'profile'
  },
  DATAMUSE_API: 'https://api.datamuse.com/words',
}
const prodConfigs= {
  API: 'http://translatefrom8.info/api/v1/',
}
const devConfigs= {
  API: 'http://localhost/api/v1/',
}


// merge env configs
const configs = Object.assign(baseConfigs, (Vue.config.productionTip)?prodConfigs:devConfigs);



Vue.use(vueConfig, configs)

export default configs;
