// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import config from './config'
import GSignInButton from 'vue-google-signin-button'
import API from './api'


Vue.use(GSignInButton)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  config,
  template: '<App/>',
  components: { App }
})

// keep reloading auth
if (localStorage.getItem('auth')!= null) {
  setTimeout(function() {
    setInterval(function(){(new API).reloadAuth()}, 1000*60*3);
  }, 1000);
}
