import Vue from 'vue'
import Router from 'vue-router'

import Index from '@/components/Index'
import AddTranslate from '@/components/AddTranslate'
import Keep from '@/components/Keep'
import Auth from '@/components/Auth'
import Translate from '@/components/Translate'
import Memory from '@/components/Memory'
import NativeLang from '@/components/NativeLang'
import Statistic from '@/components/Statistic'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: Index
    },
    {
      path: '/auth',
      name: 'auth',
      component: Auth
    },
    {
      path: '/add-translate',
      name: 'add-translate',
      component: AddTranslate
    },
    {
      path: '/keep/:page',
      name: 'keep',
      component: Keep
    },
    {
      path: '/Translate',
      name: 'translate',
      component: Translate
    },
    {
      path: '/memory',
      name: 'memory',
      component: Memory
    },
    {
      path: '/languages',
      name: 'languages',
      component: NativeLang
    },
    {
      path: '/statistic',
      name: 'statistic',
      component: Statistic
    }
  ]
})

router.beforeEach((to, from, next) => {
  let isLogin = false;

  if (localStorage.getItem('auth') != null) {
    isLogin = true;
  }

  if (!isLogin) {
    if (to.path !== '/auth') {
      return next({path: '/auth'});
    }else {
      next();
    }
  }else {
    if (to.path === '/auth') {
      return next({path: '/'});
    }
    next();
  }
})

export default router

