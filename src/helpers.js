
export function objToQuery(obj) {
  var str = [];
  for(var p in obj) {
    if (obj.hasOwnProperty(p)) {
      // console.log((obj[p]))
      if (Array.isArray(obj[p])) {
        obj[p].forEach(function(v) {
          str.push(encodeURIComponent(p) +"[]=" + v)
        });
      } else {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
      }

    }
  }
  return str.join("&");
}

export function mergeAuthQuery(query) {
  var auth = JSON.parse(localStorage.getItem("auth"));
  if (auth == null)
    return query;

  return query+'&token='+auth.token+'&token_type='+auth.type
}

export function mergeAuthObject(paramObj) {
  var auth = JSON.parse(localStorage.getItem("auth"));
  if (auth == null)
    return paramObj;

  return Object.assign(paramObj, {
    token: auth.token,
    token_type: auth.type,
  })
}




