import config from '@/config';
import {objToQuery} from '@/helpers'
import {mergeAuthQuery} from '@/helpers'
import {mergeAuthObject} from '@/helpers'

class DatamuseAPI {

  /**
   * Datamuse API
   * @param params
   * @returns {Promise.<TResult>}
   */
  static requester(params) {

    var url = config.DATAMUSE_API;
    var fetchParam = {method: 'GET'}
    var self = new DatamuseAPI();
    var isError;


    url += ('?'+mergeAuthQuery(objToQuery(params)))


    return fetch(url, fetchParam)
      .then(function(response) {
        // 對 response 做處理
        if (!response.ok) {
          // We can't get error code before json()
          isError = true;
        }
        return response.json();
      })
      .then(data => {

        // Handle global error
        if (isError) {
          self.errorHandler(data.code)
        } else {
          console.log(data)
        }

        return Promise.resolve(data)
      })
      .catch(function(error) {
        console.error(error);
      });
  }


  /**
   * Catch globally handled errors
   * @param code
   */
  errorHandler(code) {
    console.log('error : '+code)
  }

}

export {DatamuseAPI as default}
