import config from '@/config';
import {objToQuery} from '@/helpers'
import {mergeAuthQuery} from '@/helpers'
import {mergeAuthObject} from '@/helpers'
import 'whatwg-fetch'

class API {


  /**
   * Send Request to backend
   * @param uri
   * @param method
   * @param {Object} params
   * @returns {Promise<Response>}
   */
  static requester(uri, method, params) {

    var url = config.API+uri;
    var fetchParam = {method: method}
    var self = new API();
    var isError;

    if (method.toUpperCase() == 'GET' ) {
      url += ('?'+mergeAuthQuery(objToQuery(params)))
    } else {
      fetchParam.headers = {
        'Content-Type': 'application/json',
        // 'Access-Control-Allow-Origin': '*'
      };
      fetchParam.body = JSON.stringify(mergeAuthObject(params))
    }
    // console.log(fetchParam.body)

    return window.fetch(url, fetchParam)
      .then(function(response) {
        // 對 response 做處理
        if (!response.ok) {
          // We can't get error code before json()
          isError = true;
        }
        return response.json();
      })
      .then(data => {

        // Handle global error
        if (isError) {
          self.errorHandler(data.code)
        } else {
          console.log(data)
        }

        return Promise.resolve(data)
      })
      .catch(function(error) {
        console.error(error);
      });
  }

  /**
   * While token expired
   */
  reloadAuth() {

    let success = false;
    console.log('reload auth')
    setTimeout(function() {

      if (success == false) {
        console.log('auth reload time out');
        location.reload(true);
      }

    }, 5000);

    return gapi.client.init(config.GOOGLE_CLIENT).then(function () {

      var googleUser = gapi.auth2.getAuthInstance().currentUser.get();


      googleUser.reloadAuthResponse().then(function(data) {

        success = true;

        var auth = JSON.parse(localStorage.getItem("auth"));
        auth.token = data.id_token;
        localStorage.setItem("auth", JSON.stringify(auth));

        return Promise.resolve(data)
      })
      .catch(function(reason){

        console.log('google auth reload error:' + reason.type)

        localStorage.removeItem("auth");
        // window.location.href = "/#/";
        location.reload(true);
      })
      .then(function(data) {
        return Promise.resolve(data)
      })



    });
  }

  /**
   * Catch globally handled errors
   * @param code
   */
  errorHandler(code) {
    console.log('API error code: '+code)
    switch (code) {
      case 2:
        this.reloadAuth().then(function(data) {
          window.location.href = "/#/";
        })
        break;

      case 10:
        window.location.href = "/#/languages";
        break;
    }
  }

}

export {API as default}
