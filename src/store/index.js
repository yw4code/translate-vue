import Vue from 'vue'
import Vuex from 'vuex'
// import * as actions from './actions'
// import * as getters from './getters'
import translate from './modules/translate'
import search from './modules/search'
import keep from './modules/keep'
import user from './modules/user'


Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  // actions,
  // getters,
  modules: {
    translate,
    search,
    keep,
    user
  },
  strict: debug,

})
