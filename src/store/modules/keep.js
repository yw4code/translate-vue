
import API from '@/api'

// For mutations
const types = {
  LOAD_KEEPS: 'LOAD_KEEPS',
  STORE_KEEP: 'STORE_KEEP',
  REMOVE_KEEP: 'REMOVE_KEEP',
  LOAD_SCORE_STATISTIC: 'SCORE_STATISTIC',
}

const state = {
  keeps: [],
  storedKeepId:null, // int
  removedKeepId: null, // int
  scoreStatistic: null,
}

// mutations handle sync, and change state directly
const mutations = {
  [types.LOAD_KEEPS] (state, data) {
    state.keeps = data
  },
  [types.STORE_KEEP] (state, data) {
    state.storedKeepId = data
  },
  [types.REMOVE_KEEP] (state, data) {
    state.removedKeepId = data
  },
  [types.LOAD_SCORE_STATISTIC] (state, data) {
    state.scoreStatistic = data
  }
}

// actions handle async
const actions = {

  /**
   * @auth
   * @param params
   *  action
   */
  storeKeep ({commit, dispatch}, params) {

    API.requester('keep', 'post', params).then(function(data) {
      if (data.status_code>=400) {
        console.log(data.code+data.message)
      } else {
        commit(types.STORE_KEEP, data.data.keep_id)
      }
    })
  },

  /**
   *
   * @param commit
   * @param params
   *    keep_id
   */
  removeKeep({commit, dispatch}, params) {

    API.requester('keep', 'delete', params).then(function(data) {
      if (data.status_code>=400) {
        console.log(data.code+data.message)
      } else {
        commit(types.REMOVE_KEEP, data.data.keep_id)
      }
    })
  },

  /**
   *
   * @param commit
   * @param params
   *    page
   *    random
   */
  loadKeeps({commit}, params) {
    params.count=10000;
    API.requester('keep', 'get', params).then(function(data) {
      if (data.status_code>=400) {
        console.log(data.code+data.message)
      } else {
        commit(types.LOAD_KEEPS, data)
      }
    })
  },

  /**
   *
   * @param commit
   * @param params
   *    keep_id
   *    score
   */
  updateMemoryScore({commit, dispatch}, params) {
    API.requester('keep/memory-score', 'put', params).then(function(data) {
      if (data.status_code>=400) {
        console.log(data.code+data.message)
      } else {

      }
    })
  },

  loadScoreStatistic({commit}) {
    API.requester('statistic/score', 'get').then(function(data) {
      if (data.status_code>=400) {
        console.log(data.code+data.message)
      } else {
        commit(types.LOAD_SCORE_STATISTIC, data)
      }
    })
  },

}

const getters = {
  getKeeps: state => state.keeps,
  getStoredKeepId: state => state.storedKeepId,
  getRemovedKeepId: state => state.removedKeepId,
  getScoreStatistic: state => state.scoreStatistic,
}

export default {
  state,
  getters,
  actions,
  mutations
}
