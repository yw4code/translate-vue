
import config from '@/config';
import {mergeAuthQuery} from '@/helpers'
import {backendRequester} from '@/helpers'
import API from '@/api'
import DatamuseAPI from '@/datamuse'

// For mutations
const types = {
  AUTO_COMPLETE: 'AUTO_COMPLETE',
  SEARCH: 'SEARCH',
  DATAMUSE_AUTO_COMPLETE: 'DATAMUSE_AUTO_COMPLETE',
}

const state = {
  autoCompleteResult: [],
  searchResult: null,
  error: null,
  datamuseAutoCompleteResult: [],
}

// mutations handle sync, and change state directly
const mutations = {
  [types.AUTO_COMPLETE] (state, data) {
    state.autoCompleteResult = data
  },
  [types.SEARCH] (state, data) {
    state.searchResult = data
  },
  [types.DATAMUSE_AUTO_COMPLETE] (state, data) {
    state.datamuseAutoCompleteResult = data
  },
}

// actions handle async
const actions = {
  // action just receive one param
  autoComplete ({commit}, params) {

    API.requester('translate/match-keeps', 'get', params).then(function(data) {
      if (data.status_code>=400) {
        console.log(data.code+data.message)
      } else {
        commit(types.AUTO_COMPLETE, data.data)
      }
    })
  },

  /**
   * @auth
   * @param params
   *    word
   */
  searchTranslate ({commit}, params) {

    API.requester('translate/search', 'get', params).then(function(data) {
      if (data.status_code>=400) {
        console.log(data.code+data.message)
      } else {
        commit(types.SEARCH, data.data)
      }
    })
  },


  datamuseAutoComplete ({commit}, params) {
    DatamuseAPI.requester(params).then(function(data) {
      if (data.status_code>=400) {
        console.log(data.code+data.message)
      } else {
        console.log(data);
        commit(types.DATAMUSE_AUTO_COMPLETE, data)
      }
    })
  }


}

const getters = {
  getAutoComplete: state => state.autoCompleteResult,
  getSearchTranslate: state => state.searchResult,
  getSearchError: state => state.error,
  getDatamuseAutoComplete: state => state.datamuseAutoCompleteResult,
}

export default {
  state,
  getters,
  actions,
  mutations
}
