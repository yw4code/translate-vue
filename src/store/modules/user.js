
import API from '@/api'

// For mutations
const types = {
  UPDATE_NATIVE_LANG_STATUS: 'UPDATE_NATIVE_LANG_STATUS',
}

const state = {
  updateNativeLangStatus: true, // bool switcher
}

// mutations handle sync, and change state directly
const mutations = {
  [types.UPDATE_NATIVE_LANG_STATUS] (state) {
    state.updateNativeLangStatus = ! state.updateNativeLangStatus
  },

}

// actions handle async
const actions = {

  updateNativeLang({commit, dispatch}, params) {
    API.requester('user/native-lang', 'put', params).then(function(data) {
      if (data.status_code>=400) {
        console.log(data.code+data.message)
      } else {
        commit(types.UPDATE_NATIVE_LANG_STATUS)
      }
    })
  },

  triggerNativeNotSetException({commit, dispatch}, params) {
    API.requester('user/test-native-lang', 'get', params).then(function(data) {
      if (data.status_code>=400) {
        console.log(data.code+data.message)
      }
    })
  }

}

const getters = {
  updateNativeLangListener: state => state.updateNativeLangStatus,
}

export default {
  state,
  getters,
  actions,
  mutations
}
