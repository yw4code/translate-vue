
import API from '@/api'
import DatamuseAPI from '@/datamuse'

// For mutations
const types = {
  GOOGLE_TRANSLATE : 'GOOGLE_TRANSLATE',
  OXFORD_DICTIONARY : 'OXFORD_DICTIONARY',
  LOAD_TRANSLATE: 'LOAD_TRANSLATE',
  DATAMUSE_SYN: 'DATAMUSE_SYN',
  DATAMUSE_ANT: 'DATAMUSE_ANT',
}

const state = {
  googleTranslateResult: null,
  oxfordTranslateResult: null,
  translateResult: null,
  datamuseSynResult: null,
  datamuseAntResult: null,
}

// mutations handle sync, and change state directly
const mutations = {
  [types.GOOGLE_TRANSLATE] (state, data) {
    state.googleTranslateResult = data
  },
  [types.OXFORD_DICTIONARY] (state, data) {
    state.oxfordTranslateResult = data
  },
  [types.LOAD_TRANSLATE] (state, data) {
    state.translateResult = data
  },
  [types.DATAMUSE_SYN] (state, data) {
    state.datamuseSynResult = data
  },
  [types.DATAMUSE_ANT] (state, data) {
    state.datamuseAntResult = data
  }
}

// actions handle async
const actions = {

  /**
   * @auth
   * @param params
   *    en
   *    native_lang
   */
  storeTranslate ({commit}, params) {
    API.requester('translate/store', 'post', params).then(function(data) {
      if (data.status_code>=400) {
        console.log(data.code+data.message)
      } else {
        commit('STORE_KEEP', data.data.keep_id)
      }
    })
  },

  /**
   *
   * @param commit
   * @param params
   *    word
   */
  googleTranslate({commit}, params) {
    API.requester('g-translate-free', 'get', params).then(function(data) {
      if (data.status_code>=400) {
        console.log(data.code+data.message)
      } else {
        commit(types.GOOGLE_TRANSLATE, data.data)
      }
    })
  },

  /**
   *
   * @param commit
   * @param params
   *    en
   */
  oxfordDictionary({commit}, params) {
    API.requester('translate/oxford', 'get', params).then(function(data) {
      if (data.status_code>=400) {
        console.log(data.code+data.message)
      } else {
        commit(types.OXFORD_DICTIONARY, data.data)
      }
    })
  },

  /**
   *
   * @param commit
   * @param params
   *    translate_id
   */
  loadTranslate({commit}, params) {
    API.requester('translate', 'get', params).then(function(data) {
      if (data.status_code>=400) {
        console.log(data.code+data.message)
      } else {
        commit(types.LOAD_TRANSLATE, data.data)
      }
    })
  },

  /**
   *
   * @param commit
   * @param params
   *    rel_syn
   *    max
   */
  loadDatamuseSyn({commit}, params) {
    params.max = 10;
    DatamuseAPI.requester(params).then(function(data) {
      if (data.status_code>=400) {

      } else {
        commit(types.DATAMUSE_SYN, data)
      }
    })
  },
  /**
   *
   * @param commit
   * @param params
   *    rel_ant
   *    max
   */
  loadDatamuseAnt({commit}, params) {
    params.max = 10;
    DatamuseAPI.requester(params).then(function(data) {
      if (data.status_code>=400) {

      } else {
        commit(types.DATAMUSE_ANT, data)
      }
    })
  },

}

const getters = {
  getGoogleTranslate: state => state.googleTranslateResult,
  getTranslate: state => state.translateResult,
  getOxfordDictionary: state => state.oxfordTranslateResult,
  getDatamuseSyn: state => state.datamuseSynResult,
  getDatamuseAnt: state => state.datamuseAntResult,
}

export default {
  state,
  getters,
  actions,
  mutations
}
